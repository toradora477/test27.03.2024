import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Layout } from 'antd';
import Login from './components/Login.tsx';
import ChatList from './components/ChatList.tsx';
import ChatRoom from './components/ChatRoom.tsx';

import './App.css';

const { Content } = Layout;

const App: React.FC = () => {
  return (
    <div className="App">
      <Router>
        <Layout style={{ minHeight: '100vh' }}>
          <Content style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Switch>
              <Route exact path="/chatlist" component={ChatList} />
              <Route exact path="/chatroom/:id" component={ChatRoom} />
              <Route exact path="/" component={Login} />
            </Switch>
          </Content>
        </Layout>
      </Router>
    </div>
  );
};

export default App;
