import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Input, Button } from 'antd';

import { FIRE_BASE_CONFIG } from './business_constants.ts';

import firebase from 'firebase/compat/app';
import 'firebase/compat/firestore';

const ChatRoom: React.FC = () => {
  const params = useParams<{ id: string }>();
  const id = params.id;

  const [messages, setMessages] = useState<string[]>([]);
  const [newMessage, setNewMessage] = useState<string>('');

  const { TextArea } = Input;

  if (!firebase.apps.length) {
    firebase.initializeApp(FIRE_BASE_CONFIG);
  }

  const firestore = firebase.firestore();

  const sendMessage = () => {
    firestore
      .collection('messages')
      .add({
        text: newMessage,
        timestamp: firebase.firestore.FieldValue.serverTimestamp(),
      })
      .then(() => {
        console.log('Повідомлення успішно надіслано');
        setNewMessage('');
      })
      .catch((error: any) => {
        console.error('Помилка надсилання повідомлення:', error);
      });
  };

  useEffect(() => {
    const unsubscribe = firestore
      .collection('messages')
      .orderBy('timestamp', 'asc')
      .onSnapshot((snapshot: any) => {
        const fetchedMessages: string[] = [];
        snapshot.forEach((doc: any) => {
          fetchedMessages.push(doc.data().text);
        });
        setMessages(fetchedMessages);
      });
    return () => unsubscribe();
  }, []);

  return (
    <div style={{ maxWidth: '400px', margin: 'auto' }}>
      <h2>Чат з користувачем {id}</h2>
      <div>
        {messages.map((message, index) => (
          <div key={index}>{message}</div>
        ))}
      </div>
      <TextArea
        value={newMessage}
        onChange={(e) => setNewMessage(e.target.value)}
        placeholder="Введіть ваше повідомлення"
        autoSize={{ minRows: 3, maxRows: 5 }}
        style={{ marginBottom: '10px' }}
      />
      <Button type="primary" onClick={sendMessage}>
        Надіслати
      </Button>
    </div>
  );
};

export default ChatRoom;
