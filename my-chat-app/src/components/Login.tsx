import React, { useState } from 'react';
import { Input, Button, message } from 'antd';
import 'firebase/compat/auth';
import firebase from 'firebase/compat/app';
import { useHistory } from 'react-router-dom';
import { FIRE_BASE_CONFIG } from './business_constants.ts';

const Login: React.FC = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const history = useHistory();

  if (!firebase.apps.length) {
    firebase.initializeApp(FIRE_BASE_CONFIG);
  }

  const { Password } = Input;

  const handleLogin = async () => {
    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);

      history.push('/chatlist');
    } catch (error) {
      console.error('Помилка входу:', error);
      message.error('Помилка входу. Перевірте правильність введення email та пароля.');
    }
  };

  const handleRegister = async () => {
    try {
      await firebase.auth().createUserWithEmailAndPassword(email, password);
    } catch (error) {
      console.error('Помилка реєстрації:', error);
      message.error('Помилка реєстрації. Будь ласка, спробуйте ще раз.');
    }
  };

  return (
    <div style={{ maxWidth: '300px', margin: 'auto' }}>
      <Input type="email" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)} style={{ marginBottom: '10px' }} />
      <Password placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} style={{ marginBottom: '10px' }} />
      <Button type="primary" onClick={handleLogin} style={{ marginRight: '10px' }}>
        Login
      </Button>
      <Button onClick={handleRegister}>Register</Button>
    </div>
  );
};

export default Login;
