import React, { useState } from 'react';
import { List, Button } from 'antd';
import { useHistory } from 'react-router-dom';

const ChatList: React.FC = () => {
  const history = useHistory();

  const [chatList] = useState<string[]>(['Chat 1', 'Chat 2', 'Chat 3']);

  const { Item } = List;

  const openChat = (numberChat: number) => {
    console.log('Open chat:', numberChat);
    history.push(`/chatroom/${numberChat + 1}`);
  };

  return (
    <div>
      <h2>Список чатів</h2>
      <List
        dataSource={chatList}
        renderItem={(item, index) => (
          <Item key={index}>
            <Button type="primary" onClick={() => openChat(index)} children={item} />
          </Item>
        )}
      />
    </div>
  );
};

export default ChatList;
